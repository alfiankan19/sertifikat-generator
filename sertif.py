import cv2 as cv 
import openpyxl 
import img2pdf 
from PIL import Image 
import os 

template_path = 'tem.jpg'

details_path = 'daftar.xlsx'

output_path = '/home/joni/sertif'
   
font_size = 1.5
font_color = (0,0,0) 
   

coordinate_y_adjustment = 15
coordinate_x_adjustment = 7

obj = openpyxl.load_workbook(details_path) 
sheet = obj.active 

for i in range(1,500): 
       

    get_name = sheet.cell(row = i ,column = 1)
    get_owner = sheet.cell(row = i ,column = 2) 
    certi_name = get_name.value 
    certi_owner = get_owner.value
                               

    img = cv.imread(template_path) 
                                 
 
    font = cv.FONT_HERSHEY_SIMPLEX               
   

    text_size = cv.getTextSize(certi_name, font, font_size, 10)[0]      
   

    text_x = (img.shape[1] - text_size[0]) / 2 + coordinate_x_adjustment  
    text_y = (img.shape[0] + text_size[1]) / 2 - coordinate_y_adjustment 
    text_x = int(text_x) 
    text_y = int(text_y) 
    cv.putText(img, certi_name, 
              (text_x ,text_y ),  
              font, 
              font_size, 
              font_color, 2) 
   
    certi_path = output_path + '/certi_' +certi_owner+ '.png'
       
                
    cv.imwrite(certi_path,img) 

    img_path = certi_path
    
    pdf_path = output_path + '/certi_' +certi_owner+ '.pdf'
    
    image = Image.open(img_path) 
    
    pdf_bytes = img2pdf.convert(image.filename) 
    
    file = open(pdf_path, "wb") 
    
    file.write(pdf_bytes) 
    
    image.close() 
    
    file.close() 
    
    print("Successfully made pdf file") 
    print(str(i))
